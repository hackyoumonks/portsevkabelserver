# API List

### Events API

http://127.0.0.1:8000/api/port_events/ List of port events random order - GET  
http://127.0.0.1:8000/api/port_events/ordered/ List of port events ordered by date - GET  
http://127.0.0.1:8000/api/port_events/category/ List of events category filtering by title. Usage: in the end of url add ?title=category title name - GET  

***

### Users API

http://127.0.0.1:8000/api/users/ List of users with favorite events - GET  
http://127.0.0.1:8000/api/users/user_create/ User create method - POST  
http://127.0.0.1:8000/api/users/login/ User login method - POST  
