from django.contrib import admin
from .models import PortEvent, Category

models = [PortEvent, Category]

admin.site.register(models)