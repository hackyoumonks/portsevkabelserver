from rest_framework import serializers
from port_events_app.models import PortEvent, Category


class PortEventsSerializer(serializers.ModelSerializer):
    class Meta:
        model = PortEvent
        fields = '__all__'


class CategorySerializer(serializers.ModelSerializer):
    event = PortEventsSerializer(many=True, read_only=True)

    class Meta:
        model = Category
        fields = '__all__'
