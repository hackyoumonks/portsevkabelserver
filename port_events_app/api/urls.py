from django.urls import path
from .views import PortEventsViewSet, PortEventsOrderedViewSet, CategoryViewSet

urlpatterns = [
    path('', PortEventsViewSet.as_view()),
    path('ordered/', PortEventsOrderedViewSet.as_view()),
    path('category/', CategoryViewSet.as_view())
]