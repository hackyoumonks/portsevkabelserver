from .serializers import PortEventsSerializer, PortEvent, CategorySerializer, Category

from rest_framework.generics import ListAPIView


class PortEventsViewSet(ListAPIView):
    queryset = PortEvent.objects.all().order_by('?')
    serializer_class = PortEventsSerializer


class PortEventsOrderedViewSet(ListAPIView):
    queryset = PortEvent.objects.all().order_by('event_date')
    serializer_class = PortEventsSerializer


class CategoryViewSet(ListAPIView):
    model = Category
    serializer_class = CategorySerializer

    def get_queryset(self):
        queryset = Category.objects.all()
        title = self.request.query_params.get('title')
        queryset = queryset.filter(title=title)
        return queryset