from django.apps import AppConfig


class PortEventsAppConfig(AppConfig):
    name = 'port_events_app'
