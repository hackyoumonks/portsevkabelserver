from django.db import models


class PortEvent(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    event_program = models.TextField()
    event_date = models.DateTimeField(blank=True, null=True)
    img_url = models.URLField(blank=True)

    def __str__(self):
        return self.title


class Category(models.Model):
    title = models.CharField(max_length=100)
    event = models.ManyToManyField(PortEvent)

    def __str__(self):
        return self.title
