from user_auth_app.models import User
from port_events_app.api.serializers import PortEventsSerializer
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    favorites = PortEventsSerializer(many=True)

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'email', 'password', 'favorites')

    def create(self, validated_data):
        user = super(UserSerializer, self).create(validated_data)
        user.set_password(validated_data['password'])
        user.save()

        return user
