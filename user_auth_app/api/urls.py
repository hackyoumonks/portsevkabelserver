from django.urls import path, include

from .views import UserListView, CreateUserView, UserLogin

urlpatterns = [
    path('', UserListView.as_view()),
    path('<int:pk>/', UserListView.as_view()),
    path('user_create/', CreateUserView.as_view()),
    path('login/', UserLogin.as_view()),
]
