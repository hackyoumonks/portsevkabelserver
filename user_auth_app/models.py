from django.db import models
from django.contrib.auth.models import AbstractUser

from port_events_app.models import PortEvent


class User(AbstractUser):
    favorites = models.ManyToManyField(PortEvent)
